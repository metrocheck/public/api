/* global logger */
'use strict'
const moment = require('moment-timezone')

const { getPositions, getAvailableDates } = require('../model/repository/position')

module.exports.getPositions = async function (req, res) {
  let { lines, from, to } = req.params

  try {
    lines = lines.split(',').map(v => parseInt(v, 10))
    to = moment.unix(to).tz('Europe/Brussels')
    from = moment.unix(from).tz('Europe/Brussels')
  } catch (e) {
    console.error(e)
    return res.send(500)
  }

  // Maximum fetchable range : 1h
  if ((to - from) / 1000 / 60 / 60 > 1) {
    const msg = 'Maximum time range exceeded'
    logger.warn(msg)
    return res.status(400).json({ error: msg })
  }

  const positions = await getPositions(lines, from, to)
  logger.debug('getPositions : %O, %O, %O returned %O results', lines, from, to, positions.length)

  return res.json(positions)
}

module.exports.getAvailableDates = async function (req, res) {
  let lines = req.params.lines

  try {
    lines = lines.split(',').map(v => parseInt(v, 10))
  } catch (e) {
    console.error(e)
    return res.send(500)
  }

  const dates = await getAvailableDates(lines)
  // console.debug(dates)

  return res.json(dates)
}
