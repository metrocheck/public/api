'use strict'
const { getPoints } = require('../model/repository/point')

module.exports.getPoints = async function (req, res) {
  const points = await getPoints()
  // const geojson = GeoJSON.parse(points, {Point: ['geoloc.x', 'geoloc.y'], include: ['name', 'id']});
  // return res.json({type: 'geojson', data: geojson})

  return res.json(points)
}
