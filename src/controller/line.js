'use strict'

const moment = require('moment')

const { getLines, getLineServiceFrequency } = require('../model/repository/line')

module.exports.getLines = async function (req, res) {
  const lines = await getLines()
  return res.json(lines)
}

module.exports.getStatistics = async function (req, res) {
  const line = req.params.line
  const date = moment(req.params.date)

  try {
    const frequency = await getLineServiceFrequency(line, date)

    /* todo: cache should be handled better */
    return res.header('Cache-Control: max-age=3600').json({
      frequency
    })
  } catch (e) {
    console.error(e)
    return res.status(500).send(e.toString())
  }
}
