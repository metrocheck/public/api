'use strict'
const { getLinePoints } = require('../model/repository/line_point')

module.exports.getLinePoints = async function (req, res) {
  const lines = await getLinePoints()
  return res.json(lines)
}
