/* global logger */
const mysql = require('mysql')

let pool = null

module.exports = function (app) {
  if (!pool) {
    const { host, user, password, database } = app.config.db

    pool = mysql.createPool({
      host,
      user,
      password,
      database,
      // debug: true,
      // This is needed for queries such as getLineDirections
      multipleStatements: true
    })

    logger.info('Created Mysql connection instance')
  }

  return pool
}
