'use strict'

const { query } = require('../../util/db')
const db = require('../../db')
const { mariadbDateFormat } = require('../../util/db')

module.exports = {
  getLines: async () => {
    const sql = 'SELECT * from line'
    const lines = await query(db(), sql)

    return lines
  },

  getLineDirections: async (line) => {
    // console.debug('getLineDirections : %O', line)

    const sql = `SET @total=0;
SELECT id, COUNT( id ) / @total * 100 AS percentage, name_fr
FROM (
  SELECT p.direction_fk as id, po.name_fr as name_fr, @total := @total + 1
     FROM point po
            RIGHT JOIN position p ON p.direction_fk = po.id
     WHERE p.line_fk = ?
   ) temp
 GROUP BY id;`

    const result = await query(db(), sql, [line])
    return result[1]
  },

  /* Stats */

  /* Statistics */
  getLineServiceFrequency: async (line, date) => {
    // console.debug('getLineServiceFrequency : %O', line)
    const sql = `
            SELECT DATE_FORMAT(
                           MIN(datetime),
                           '%Y-%m-%d %H:%i:00'
                       )     AS datetime,
#                    COUNT(id) AS recordsNumber,
#                    COUNT(DISTINCT datetime) as timeOccurence,
                   COUNT(id)/COUNT(DISTINCT datetime) AS value
            FROM position
            WHERE line_fk = ?
              AND DATE(datetime) = ?
            GROUP BY ROUND(UNIX_TIMESTAMP(datetime) / 600)
            ORDER BY datetime
            ;
        `
    const result = await query(db(), sql, [line, date.format(mariadbDateFormat)])
    return result
  },
}
