'use strict'

const { query } = require('../../util/db')
const db = require('../../db')
const { mysqlDatetimeFormat } = require('../../util/db')

module.exports = {
  getPositions: async (lines, from, to) => {
    const _from = from.utc().format(mysqlDatetimeFormat)
    const _to = to.utc().format(mysqlDatetimeFormat)

    const sql = `SELECT p.*, po.geoloc as point_geolocation
                     FROM position p
                              JOIN point po ON p.point_fk = po.id
                     WHERE p.line_fk IN (?)
                       AND p.datetime BETWEEN ? AND ?
                     ORDER BY p.datetime, p.direction_fk`

    const positions = await query(db(), sql, [lines, _from, _to])
    return positions
  },

  getAvailableDates: async (lines) => {
    // console.debug('getAvailableDates : %O', lines)
    const sql = `SELECT DISTINCT DATE(datetime) as date
                     FROM position
                     WHERE line_fk IN (?)`

    const dates = await query(db(), sql, [lines])

    return dates.map(res => res.date)
  }

}
