'use strict'

const { query } = require('../../util/db')
const db = require('../../db')

module.exports = {
  getLinePoints: async () => {
    const sql = 'SELECT * from line_point'
    const linePoints = await query(db(), sql)

    return linePoints
  }
}
