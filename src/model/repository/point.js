'use strict'

const { query } = require('../../util/db')
const db = require('../../db')

module.exports = {
  getPoints: async () => {
    const sql = 'SELECT id, name_fr, ST_Y(geoloc) as lon, ST_X(geoloc) as lat FROM point p'
    // noinspection UnnecessaryLocalVariableJS
    const points = await query(db(), sql)

    return points
  },
}
