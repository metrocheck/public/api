const base = require('./base')
const secret = process.env.SECRET || 'dz6r4g6uy4lioyt4ju3i5vex0z.2tyh056ye'
const env = process.env.NODE_ENV || 'production'

module.exports = Object.assign(base, {
  secret: secret,
  env: env
})
