const secrets = require('../util/secrets')

module.exports = {
  db: {
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: secrets.get(process.env.DB_PASSWORD_FILE),
    database: process.env.DB_DATABASE
  },

  logDirectory: '/var/log/metrocheck/api'
}
