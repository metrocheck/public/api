const base = require('./base')
const secret = process.env.SECRET || 'ef516t85g16r5z1e3f5y1tj35ui1531t'
const env = process.env.NODE_ENV || 'dev'

module.exports = Object.assign(base, {
  secret: secret,
  env: env,
  logDirectory: 'logs'
})
