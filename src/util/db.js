'use strict'

module.exports = {
  query: (connection, sql, parameters = null) => new Promise(
    (resolve, reject) =>
      connection.query(sql, parameters, function (error, results, fields) {
        if (error) return reject(error)
        else return resolve(results)
      })),

  mariadbDateFormat: 'YYYY-MM-DD HH:mm:ss',
  mariadbDatetimeFormat: 'YYYY-MM-DD'
}
