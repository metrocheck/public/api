const { existsSync, mkdirSync } = require('fs')
const express = require('express')
const { join } = require('path')
const routes = require('./routes')
const bodyParser = require('body-parser')
const winston = require('winston')
const config = require('./config')(process.env.NODE_ENV)
// const cors = require('cors')

const app = express()
const { combine, timestamp, printf, splat } = winston.format
// app.use(cors())

global.__root = join(__dirname, '/')
global.HttpError = require('./util/HttpError')
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

if (!existsSync(config.logDirectory)) mkdirSync(config.logDirectory)

/* global logger */
global.logger = winston.createLogger({
  format: combine(
    timestamp(),
    printf(info => `${info.timestamp} ${info.level}: ${info.message}`),
    splat()
  ),
  transports: [
    new winston.transports.File({ dirname: config.logDirectory, filename: 'error.log', level: 'error' }),
    new winston.transports.File({ dirname: config.logDirectory, filename: 'combined.log' })]
})

routes(app)
/* @todo: test 404 */

module.exports = {
  serve (env) {
    if (env !== 'test') { logger.add(new winston.transports.Console({ prettyPrint: true })) }

    app.config = require('./config')(env)
    require('./db')(app)
    require('./routes')(app)
    app.port = process.env.PORT || 4000
    return new Promise((resolve, reject) => {
      app.listen(app.port, function (err) {
        if (err) { return reject(err) }
        resolve(app)
      })
    })
  }
}
