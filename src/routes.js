'use strict'
/* global logger */
const pointController = require('./controller/point')
const positionController = require('./controller/position')
const lineController = require('./controller/line')
const linePointController = require('./controller/line_point')

module.exports = function (app) {
  const express = require('express')
  const router = express.Router()

  /**
   * default error handling
   */
  function errorHandler (err, req, res, next) {
    console.log('errorHandler')
    logger.error(err.stack)
    res.status(err.status || 500).send(err.message)
    next()
  }

  router.use(errorHandler)

  app.get('/lines', lineController.getLines)

  app.get('/points/:lines', pointController.getPoints)

  app.get('/line-points/', linePointController.getLinePoints)

  app.get('/positions/:lines/:from/:to', positionController.getPositions)

  app.get('/dates/:lines', positionController.getAvailableDates)

  /* Statistics */
  app.get('/statistics/line/:line/:date', lineController.getStatistics)

  /* 404 */
  app.use(function (req, res, next) {
    res.status(404).json('404')
  })
}
