# Metrocheck API
NodeJs (Express) API serving data for [metrocheck client](https://gitlab.com/metrocheck/public/client)

## Get started
Run the application with docker

Example `docker-compose` configuration : 
```dockerfile
version: '3.7'
services:
  api:
    build:
      context: <API_DIRECTORY>
      dockerfile: Dockerfile.dev
    environment:
      - WAIT_HOSTS=<DB_HOST:DB_PORT>
      - WAIT_BEFORE_HOSTS=5
      - DB_HOST=<DB_HOST>
      - DB_USER=<DB_USER>
      - DB_PASSWORD_FILE=<DB_PASSWORD_FILE>
      - DB_DATABASE=<DB_DATABASE_NAME>
    secrets:
      - <DB_PASSWORD_SECRET>
    depends_on:
      - <DB_SERVICE_NAME>
    ports:
      - '4000:4000'
    volumes:
      - '<API_DIRECTORY>:/usr/src/app'
    networks:
      - db
```

### Endpoints

  ####`/lines`
  Get lines and their color
  
  Returns `{ id: number, color: string }[]`
  
  ####`/points/:lines`
  Get points on a line
  
  Returns `{ id: number, name_fr: string, lon: number, lat: number }[]`

  ####`/line-points/`
  Get stations order. Useful to calculate in-transit positions
  
  Returns `{ line_fk: number, point_fk: number, direction_name_fr: string, order: number }[]`

  ####`/positions/:lines/:from/:to`
  Get positions by line and temporal bounds

  Returns 
```typescript
  interface response { 
      id: number,
      datetime: string,
      line_fk: number,
      direction_fk: number,
      distance_from_point: number,
      point_fk: number, 
      point_geolocation: { x: number, y: number }
  }[]
```  

  ####`/dates/:lines`
  Get dates with data, by lines

  Returns `string[]`


  ####`/statistics/line/:line/:date`
  Get statistics by date and line
  
  Returns `{ frequency: { datetime: string, value: string }[] }`
  
### Endpoints parameters

| name  | type                  | example    |  
|------ | --------------------- | ---------- |
| lines | comma-separated array | 1,2,3,4    |
| from  | unix time             | 1593355091 |
| to    | unix time             | 1593355091 |
| date  | date : YYYY-MM-DD     | 2020-01-01 |

## Npm commands
These commands are available : 
* `audit-deps`: Used in CI. Generate artifacts.
* `audit-licences`: Used in CI.  Generate artifacts.
* `commit`: Call commitizen to commit
* `dev`: Nodemon dev server
* `lint`: Run eslint on project
* `prod`: Node app.js
* `start`: = prod
* `watch`: Nodemon Lint & start

## License
ISC
