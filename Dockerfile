# build stage
FROM node:lts-alpine as builder
WORKDIR /usr/src/app
ENV NODE_ENV="production"

RUN apk add yarn

COPY package*.json ./
COPY *.lock ./

RUN set -ex; yarn install

# production stage
FROM node:lts-alpine as production
ENV NODE_ENV="production"
WORKDIR /usr/src/app

# Add wait script to the image
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.5.0/wait /wait
RUN chmod +x /wait

COPY --from=builder /usr/src/app/node_modules ./node_modules
COPY . .

EXPOSE 4000

CMD /wait && npm run prod
